#* @vtlvariable name="item" type="org.nrg.xft.XFTItem" *#
#* @vtlvariable name="user" type="org.nrg.xdat.security.XDATUser" *#
#* @vtlvariable name="siteConfig" type="org.nrg.xdat.preferences.SiteConfigPreferences" *#
#* @vtlvariable name="turbineUtils" type="org.nrg.xdat.turbine.utils.TurbineUtils" *#
#* @vtlvariable name="om" type="org.nrg.xdat.om.XnatImagesessiondata" *#
#* @vtlvariable name="scan" type="org.nrg.xdat.om.XnatImagescandata" *#
#* @vtlvariable name="resource" type="org.nrg.xdat.om.XnatAbstractresource" *#
<!--BEGIN /screens/xnat_imageSessionData/xnat_imageSessionData_scans.vm -->
#macro(renderEmptyResourceCount)
    #if ($data.getSession().getAttribute("userHelper").canEdit($item))
    <a onclick="window.viewer.init(true);">Show Counts</a>
    #else
        #displayListAsTipText(["File Count Unknown", "No files were found associated with this scan. Contact a member or owner of this project to generate the file count data."])
    #end
#end

<div class="data-table-container" style="width: #if($siteConfig.uiDisplaySeriesDescription) 1089px #else 889px #end" id="selectable-table-scans">
    <div class="data-table-titlerow">
        <h3 class="data-table-title">Scans</h3>
        <div class="data-table-actionsrow">
            <span class="textlink-sm data-table-action">
                Bulk Actions:
            </span>
            <button class="btn btn-sm data-table-action disabled">
                Open In XImg Viewer
            </button>
            <button class="btn btn-sm data-table-action disabled" onclick="downloadScans({ projectId: '$om.getProject()', accessionId: '$om.getId()', xsiType: '$om.getXSIType()'})">
                Download
            </button>
            #addGlobalCustomScreens("xnat_imageScanData/scanActions")
        </div>
        <span class="clear clearfix"></span>
    </div>
    #set($scanAssessors =$!om.getScanAssessors())

    <div class="data-table-wrapper no-body" style="padding-right: 0">
        <table class="xnat-table clean fixed-header" style="border:none;">
            <thead>
            <tr>
                <th class="toggle-all" style="width: 45px;">
                    <input type="checkbox" class="selectable-select-all" id="toggle-all-scans" title="Toggle All Scans" />
                </th>
                <th class="left" style="width: 120px">Scan</th>
                <th class="left" style="width: 200px;">Type</th>
                #if($siteConfig.uiDisplaySeriesDescription)
                    <th class="left" style="width: 200px;">Series Desc</th>
                    #set($fileIndex="4")
                #else
                    #set($fileIndex="3")
                #end
                <th class="left" style="width: 125px;">Usability</th>
                <th class="left" style="width: 160px;">Files</th>
                <th class="left" style="width: 200px;">Note</th>
                <th style="width: 24px;">Run</th>
            </tr>
            </thead>
        </table>
    </div>
    <script type="text/javascript">
        var jsScanList = [];
    </script>
    <div class="data-table-wrapper no-header" style="/* max-height: 320px; overflow-y: auto */">
        <table class="xnat-table clean selectable" style="border: none;">
            <tbody>

            #set ($scanWFiles = 0)
            #if ($om.getSortedScans().isEmpty())
                <tr valign="top">
                    <td colspan="6"><em>No scans found in this session.</em></td>
                </tr>
            #else
                #foreach($scan in $om.getSortedScans())
                    #set ($scanID =$!scan.getProperty("ID"))
                    <script type="text/javascript">
                        jsScanList.push({ id: '$scanID', series_description: '$!scan.getProperty("series_description")' });
                    </script>

                    <tr valign="top" id="scan-$scanID">
                        <td class="scan-actions-controls scan-selector center" style="width: 45px;">
                            <input type="checkbox" class="selectable-select-one" name="scanAction" value="$scanID" />
                        </td>
                        <td class="scan-${scanID}-id" style="width: 120px;">
                            <span class="inline-actions">
                                <i class="fa fa-list-alt" onclick="displayScanDetails($scanID)" title="View Details"></i>
                                <i class="fa fa-eye" onclick="launchXImgView('/REST/projects/$om.getProject()/subjects/$om.getSubjectId()/experiments/$om.getId()/scans/$scanID')" title="Open Scan in Image Viewer"></i>
                                <i class="fa fa-download" title="Download" onclick="window.location.assign('/app/action/XDATActionRouter/xdataction/XDATScreen_download_sessions/search_element/$om.getXSIType()/search_field/${om.getXSIType()}ID/search_value/$!om.getId()/popup/false/project/$om.getProject()')"></i>
                                #if ($data.getSession().getAttribute("userHelper").canDelete($item))
                                    <i class="fa fa-trash" title="Delete Scan $scanID" onclick="deleteScan('$scanID')"></i>
                                #end
                            </span>
                            <a href="#!" onclick="displayScanDetails($scanID)">
                                $scanID
                            </a>
                        </td>
                        <td class="scan-${scanID}-type" style="width: 200px;">
                            <span class="truncateCell truncate175" title="$!scan.getProperty("type")">$!scan.getProperty("type")</span>
                        </td>
                        #if($siteConfig.uiDisplaySeriesDescription)
                            <td class="scan-${scanID}-description" style="width: 200px;">
                                <span class="truncateCell truncate175" title="$!scan.getProperty("series_description")">$!scan.getProperty("series_description")</span>
                            </td>
                        #end
                        <td class="scan-${scanID}-quality left quality-$!scan.getProperty("quality")" style="font-weight:bold; width: 125px" >
                            <span class="truncateCell truncateCellNarrow" title="$!scan.getProperty("quality")">$!scan.getProperty("quality")</span>
                        </td>

                        <td class="scan-${scanID}-files" style="width: 160px;">
                            #if ($data.getSession().getAttribute("userHelper").canEdit($item))
                                <span class="inline-actions">
                                    <i class="fa fa-upload uploadLink" title="Upload Resource" data-type="xnat:imageScanData" data-props="$!scan.getType()" data-uri="$content.getURI("/data/experiments/$om.getId()/scans/$scan.getId()")"></i>
                                </span>
                            #end
                            #if($scan.getFile().size()>0)

                                #set($stats = $!scan.getReadableFileStats())
                                #if("$!stats.get(0)" == "" || $stats.size() == 0 || $stats.get(0).equals("0 B in 0 files"))
                                    #renderEmptyResourceCount()
                                #elseif($stats.size() == 1)
                                    $stats.get(0)
                                #else
                                    #displayListAsTipText($stats)
                                #end

                                #set ($scanWFiles = $scanWFiles+1)
                            #else
                                #renderEmptyResourceCount()
                            #end
                        </td>
                        <td class="scan-${scanID}-note">
                            <span class="inline-actions">
                                <i class="fa fa-edit" title="Edit Note" onclick="editScanNote('$scanID')"></i>
                            </span>
                            <span class="truncate truncate175" title="$!scan.getProperty("note")">
                                $!scan.getProperty("note")
                            </span>
                        </td>
                        <td style="position: relative; width: 24px;">
                            <div class="inline-actions-menu-toggle hidden"><i class="fa fa-caret-right"></i></div>
                            <ul class="inline-actions-menu single-scan-actions-menu" style="display:none;"></ul>
                        </td>
                    </tr>
                #end

                <script>
                    var XNAT = getObject(XNAT || {});
                    XNAT.data = getObject(XNAT.data || {});
                    XNAT.data.context = getObject(XNAT.data.context || {});
                    XNAT.data.context.scans = jsScanList;
                </script>
            #end
            </tbody>

        </table>
    </div>
    #if($scanWFiles>0)
    <div class="data-table-footer">
        <span id="total_dicom_files">
            <b>Total:</b>
            #set($sessionStats = $om.getSessionReadableScanStats())
            #if("$!sessionStats.get(0)" == "" || $sessionStats.size() == 0 || $sessionStats.get(0).equals("0 B in 0 files"))
                #renderEmptyResourceCount()
            #else
                #displayListAsTipText($sessionStats)
            #end
        </span>
    </div>
    #end
</div>

<!-- hidden: scan details -->

#foreach($scan in $om.getSortedScans())
    #set ($scanID =$!scan.getProperty("ID"))
    <div class="html-template" id="scan-${scanID}-details">
        #parse($turbineUtils.getTemplateName("_details",$scan.getXSIType(),$project))
    </div>
#end


<script>
    (function(){
        /* inline scan table functions */
        window.displayScanDetails = function (scanId) {
            if (!scanId) return false;

            if (loadSnapshotImageNoBlocking(scanId)) {
                var tmpl = $('#scan-' + scanId + '-details').html();
                
                XNAT.ui.dialog.open({
                    title: 'Scan ' + scanId,
                    width: 480,
                    content: '<div class="panel"></div>',
                    beforeShow: function (obj) {
                        var container = obj.$modal.find('.panel');
                        container.append(tmpl);
                    },
                    isDraggable: true,
                    mask: false,
                    footer: {
                        content: 'Click in the header to move this dialog around the page'
                    }
                });
            }
        };

        window.editScanNote = function (scanId) {
            var xsiType = XNAT.data.context.xsiType;
            var noteText = $('.scan-' + scanId + '-note').find('.truncate').html().trim();
            XNAT.ui.dialog.open({
                title: 'Edit Note for Scan ' + scanId,
                width: 480,
                nuke: true,
                content: '<div class="panel"><form id="scan-note-editor"></form></div>',
                beforeShow: function (obj) {
                    obj.$modal.find('#scan-note-editor')
                            .append(XNAT.ui.panel.textarea({
                                name: xsiType + '/scans/scan[ID=' + scanId + ']/note',
                                label: 'Note',
                                value: noteText,
                                code: 'html'
                            }).spawned);
                },
                buttons: [
                    {
                        label: 'Update Note',
                        isDefault: true,
                        close: false,
                        action: function (obj) {
                            var updateNoteUrl = '/REST/projects/' + XNAT.data.context.projectID + '/subjects/' + XNAT.data.context.subjectID + '/experiments/' + XNAT.data.context.ID + '?req_format=form';
                            var updateNoteString = obj.$modal.find('#scan-note-editor').serialize();
                            updateNoteString = updateNoteString || null;
                            XNAT.xhr.put({
                                url: XNAT.url.csrfUrl(updateNoteUrl),
                                data: updateNoteString,
                                success: function (data) {
                                    XNAT.ui.dialog.open({
                                        title: 'Success',
                                        width: 360,
                                        content: '<p>Scan note updated. Page will reload.</p>',
                                        buttons: [
                                            {
                                                label: 'OK',
                                                isDefault: true,
                                                close: true,
                                                action: function () {
                                                    XNAT.ui.dialog.closeAll();
                                                    window.location.reload();
                                                }
                                            }
                                        ],
                                    })
                                },
                                fail: function (e) {
                                    XNAT.ui.dialog.open({
                                        title: 'Error',
                                        width: 360,
                                        content: '<p><strong>Error ' + e.status + '</strong></p><p>' + e.statusText + '</p>',
                                        buttons: [
                                            {
                                                label: 'OK',
                                                isDefault: true,
                                                close: true
                                            }
                                        ]
                                    })
                                }
                            })
                        }
                    },
                    {
                        label: 'Cancel',
                        isDefault: false,
                        close: true
                    }
                ]
            });
        }

        window.deleteScan = function (scanId) {
            XNAT.ui.dialog.open({
                title: 'Delete Scan ' + scanId,
                width: 480,
                nuke: true, // destroys the modal on close, rather than preserving its state
                content: '<div class="panel"></div>',
                beforeShow: function (obj) {
                    obj.$modal.find('.panel')
                            .append('<p>This will delete scan ' + scanId + ' from this image session. You can choose whether to delete all scan files from the filesystem as well. </p>')
                            .append(XNAT.ui.panel.input.switchbox({
                                name: 'delete_files',
                                id: 'delete_files',
                                kind: 'switchbox',
                                value: 'true',
                                label: 'Delete Scan Files',
                                onText: 'Yes (Default)',
                                offText: 'No'
                            }));
                },
                buttons: [
                    {
                        label: 'Delete Scan',
                        isDefault: true,
                        close: false,
                        action: function (obj) {
                            var deleteUrl = '/REST/experiments/' + XNAT.data.context.ID + '/scans/' + scanId;
                            var deleteFiles = obj.$modal.find('#delete_files').is(':checked');
                            var params = '?format=json';
                            params += '&event_action=Removed scan';
                            params += '&event_type=WEB_FORM';
                            if (deleteFiles) {
                                params += '&removeFiles=true';
                            }
                            XNAT.xhr.delete({
                                url: XNAT.url.rootUrl(deleteUrl + params),
                                success: function (data) {
                                    XNAT.ui.dialog.open({
                                        title: 'Success',
                                        width: 360,
                                        content: '<p>Scan Deleted. Page will reload.</p>',
                                        buttons: [
                                            {
                                                label: 'OK',
                                                isDefault: true,
                                                close: true,
                                                action: function () {
                                                    XNAT.ui.dialog.closeAll();
                                                    window.location.reload();
                                                }
                                            }
                                        ],
                                    })
                                },
                                fail: function (e) {
                                    XNAT.ui.dialog.open({
                                        title: 'Error',
                                        width: 360,
                                        content: '<p><strong>Error ' + e.status + '</strong></p><p>' + e.statusText + '</p>',
                                        buttons: [
                                            {
                                                label: 'OK',
                                                isDefault: true,
                                                close: true
                                            }
                                        ]
                                    })
                                }
                            })
                        }
                    },
                    {
                        label: 'Cancel',
                        isDefault: false,
                        close: true
                    }
                ]
            });
        }

        window.downloadScans = function(params){
            var url = '/app/action/XDATActionRouter/xdataction/XDATScreen_download_sessions/search_element/'+params.xsiType+'/search_field/'+params.xsiType+'ID/search_value/'+params.accessionId+'/popup/false/project/'+params.projectId;
            window.location.assign(XNAT.url.rootUrl(url));
        }

        function loadSnapshotImageNoBlocking(scanID) {
            var element = $("#" + "scan" + scanID + "snapshot");
            element[0].src = element.data("xnat-src");
            return true;
        }
    })();

</script>
<script src="$content.getURI("scripts/xnat/plugin/scanTable/selectableTableBehavior.js")"></script>
<link rel="stylesheet" href="$content.getURI("scripts/xnat/plugin/scanTable/scanTable.css")"></link>

<BR/>
<BR/>



#set ($qcXsiType = "xnat:qcAssessmentData")
#parse($turbineUtils.getTemplateName("report_summary",$qcXsiType,$project))
<!--END /screens/xnat_imageSessionData/xnat_imageSessionData_scans.vm -->
