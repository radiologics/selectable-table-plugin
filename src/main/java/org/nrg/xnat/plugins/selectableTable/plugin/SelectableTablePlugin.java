/*
 * xnat-selectable-table: org.nrg.xnat.plugins.selectableTable.plugin.SelectableTablePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins.selectableTable.plugin;

import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "selectableTablePlugin", name = "XNAT 1.7 Selectable Table Plugin", description = "Replaces the default XNAT scan table for image sessions with a version that supports inline and bulk actions.")

public class SelectableTablePlugin {
}
